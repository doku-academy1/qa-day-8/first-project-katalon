<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Get Single User with id 2</name>
   <tag></tag>
   <elementGuidId>f8992a1a-b219-4c92-b11f-ed5a01eccdbc</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <katalonVersion>8.5.2</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>https://reqres.in/api/users/2</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

String jsonPass =
&quot;&quot;&quot;
{
  &quot;\$schema&quot;: &quot;http://json-schema.org/draft-04/schema#&quot;,
  &quot;type&quot;: &quot;object&quot;,
  &quot;properties&quot;: {
    &quot;data&quot;: {
      &quot;type&quot;: &quot;object&quot;,
      &quot;properties&quot;: {
        &quot;id&quot;: {
          &quot;type&quot;: &quot;integer&quot;
        },
        &quot;email&quot;: {
          &quot;type&quot;: &quot;string&quot;
        },
        &quot;first_name&quot;: {
          &quot;type&quot;: &quot;string&quot;
        },
        &quot;last_name&quot;: {
          &quot;type&quot;: &quot;string&quot;
        },
        &quot;avatar&quot;: {
          &quot;type&quot;: &quot;string&quot;
        }
      },
      &quot;required&quot;: [
        &quot;id&quot;,
        &quot;email&quot;,
        &quot;first_name&quot;,
        &quot;last_name&quot;,
        &quot;avatar&quot;
      ]
    },
    &quot;support&quot;: {
      &quot;type&quot;: &quot;object&quot;,
      &quot;properties&quot;: {
        &quot;url&quot;: {
          &quot;type&quot;: &quot;string&quot;
        },
        &quot;text&quot;: {
          &quot;type&quot;: &quot;string&quot;
        }
      },
      &quot;required&quot;: [
        &quot;url&quot;,
        &quot;text&quot;
      ]
    }
  },
  &quot;required&quot;: [
    &quot;data&quot;,
    &quot;support&quot;
  ]
}
&quot;&quot;&quot;

boolean successful = WS.validateJsonAgainstSchema(response,jsonPass)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
