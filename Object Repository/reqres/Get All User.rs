<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Get All User</name>
   <tag></tag>
   <elementGuidId>a3d8e33a-2867-47c0-a9b4-24adefdc8927</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <katalonVersion>8.5.2</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>https://reqres.in/api/unknown</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

String jsonPass =
&quot;&quot;&quot;
{
  &quot;\$schema&quot;: &quot;http://json-schema.org/draft-04/schema#&quot;,
  &quot;type&quot;: &quot;object&quot;,
  &quot;properties&quot;: {
    &quot;page&quot;: {
      &quot;type&quot;: &quot;integer&quot;
    },
    &quot;per_page&quot;: {
      &quot;type&quot;: &quot;integer&quot;
    },
    &quot;total&quot;: {
      &quot;type&quot;: &quot;integer&quot;
    },
    &quot;total_pages&quot;: {
      &quot;type&quot;: &quot;integer&quot;
    },
    &quot;data&quot;: {
      &quot;type&quot;: &quot;array&quot;,
      &quot;items&quot;: [
        {
          &quot;type&quot;: &quot;object&quot;,
          &quot;properties&quot;: {
            &quot;id&quot;: {
              &quot;type&quot;: &quot;integer&quot;
            },
            &quot;name&quot;: {
              &quot;type&quot;: &quot;string&quot;
            },
            &quot;year&quot;: {
              &quot;type&quot;: &quot;integer&quot;
            },
            &quot;color&quot;: {
              &quot;type&quot;: &quot;string&quot;
            },
            &quot;pantone_value&quot;: {
              &quot;type&quot;: &quot;string&quot;
            }
          },
          &quot;required&quot;: [
            &quot;id&quot;,
            &quot;name&quot;,
            &quot;year&quot;,
            &quot;color&quot;,
            &quot;pantone_value&quot;
          ]
        },
        {
          &quot;type&quot;: &quot;object&quot;,
          &quot;properties&quot;: {
            &quot;id&quot;: {
              &quot;type&quot;: &quot;integer&quot;
            },
            &quot;name&quot;: {
              &quot;type&quot;: &quot;string&quot;
            },
            &quot;year&quot;: {
              &quot;type&quot;: &quot;integer&quot;
            },
            &quot;color&quot;: {
              &quot;type&quot;: &quot;string&quot;
            },
            &quot;pantone_value&quot;: {
              &quot;type&quot;: &quot;string&quot;
            }
          },
          &quot;required&quot;: [
            &quot;id&quot;,
            &quot;name&quot;,
            &quot;year&quot;,
            &quot;color&quot;,
            &quot;pantone_value&quot;
          ]
        },
        {
          &quot;type&quot;: &quot;object&quot;,
          &quot;properties&quot;: {
            &quot;id&quot;: {
              &quot;type&quot;: &quot;integer&quot;
            },
            &quot;name&quot;: {
              &quot;type&quot;: &quot;string&quot;
            },
            &quot;year&quot;: {
              &quot;type&quot;: &quot;integer&quot;
            },
            &quot;color&quot;: {
              &quot;type&quot;: &quot;string&quot;
            },
            &quot;pantone_value&quot;: {
              &quot;type&quot;: &quot;string&quot;
            }
          },
          &quot;required&quot;: [
            &quot;id&quot;,
            &quot;name&quot;,
            &quot;year&quot;,
            &quot;color&quot;,
            &quot;pantone_value&quot;
          ]
        },
        {
          &quot;type&quot;: &quot;object&quot;,
          &quot;properties&quot;: {
            &quot;id&quot;: {
              &quot;type&quot;: &quot;integer&quot;
            },
            &quot;name&quot;: {
              &quot;type&quot;: &quot;string&quot;
            },
            &quot;year&quot;: {
              &quot;type&quot;: &quot;integer&quot;
            },
            &quot;color&quot;: {
              &quot;type&quot;: &quot;string&quot;
            },
            &quot;pantone_value&quot;: {
              &quot;type&quot;: &quot;string&quot;
            }
          },
          &quot;required&quot;: [
            &quot;id&quot;,
            &quot;name&quot;,
            &quot;year&quot;,
            &quot;color&quot;,
            &quot;pantone_value&quot;
          ]
        },
        {
          &quot;type&quot;: &quot;object&quot;,
          &quot;properties&quot;: {
            &quot;id&quot;: {
              &quot;type&quot;: &quot;integer&quot;
            },
            &quot;name&quot;: {
              &quot;type&quot;: &quot;string&quot;
            },
            &quot;year&quot;: {
              &quot;type&quot;: &quot;integer&quot;
            },
            &quot;color&quot;: {
              &quot;type&quot;: &quot;string&quot;
            },
            &quot;pantone_value&quot;: {
              &quot;type&quot;: &quot;string&quot;
            }
          },
          &quot;required&quot;: [
            &quot;id&quot;,
            &quot;name&quot;,
            &quot;year&quot;,
            &quot;color&quot;,
            &quot;pantone_value&quot;
          ]
        },
        {
          &quot;type&quot;: &quot;object&quot;,
          &quot;properties&quot;: {
            &quot;id&quot;: {
              &quot;type&quot;: &quot;integer&quot;
            },
            &quot;name&quot;: {
              &quot;type&quot;: &quot;string&quot;
            },
            &quot;year&quot;: {
              &quot;type&quot;: &quot;integer&quot;
            },
            &quot;color&quot;: {
              &quot;type&quot;: &quot;string&quot;
            },
            &quot;pantone_value&quot;: {
              &quot;type&quot;: &quot;string&quot;
            }
          },
          &quot;required&quot;: [
            &quot;id&quot;,
            &quot;name&quot;,
            &quot;year&quot;,
            &quot;color&quot;,
            &quot;pantone_value&quot;
          ]
        }
      ]
    },
    &quot;support&quot;: {
      &quot;type&quot;: &quot;object&quot;,
      &quot;properties&quot;: {
        &quot;url&quot;: {
          &quot;type&quot;: &quot;string&quot;
        },
        &quot;text&quot;: {
          &quot;type&quot;: &quot;string&quot;
        }
      },
      &quot;required&quot;: [
        &quot;url&quot;,
        &quot;text&quot;
      ]
    }
  },
  &quot;required&quot;: [
    &quot;page&quot;,
    &quot;per_page&quot;,
    &quot;total&quot;,
    &quot;total_pages&quot;,
    &quot;data&quot;,
    &quot;support&quot;
  ]
}
&quot;&quot;&quot;

boolean successful = WS.validateJsonAgainstSchema(response,jsonPass)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
